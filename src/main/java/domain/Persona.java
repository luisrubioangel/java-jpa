package domain;

public class Persona {
	private int id_Persona;
	private String nombre;
	private String apellido;
	private String email;
	private int telefono;
	 
	public Persona() {		
	}

	public Persona(int id_Persona) {	
		this.id_Persona = id_Persona;
	}

	public Persona(String nombre, String apellido, String email, int telefono) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.email = email;
		this.telefono = telefono;
	}


	public Persona(int id_Persona, String nombre, String apellido, String email, int telefono) {
		super();
		this.id_Persona = id_Persona;
		this.nombre = nombre;
		this.apellido = apellido;
		this.email = email;
		this.telefono = telefono;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getApellido() {
		return apellido;
	}


	public void setApellido(String apellido) {
		this.apellido = apellido;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public int getTelefono() {
		return telefono;
	}


	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}


	public int getId_Persona() {
		return this.id_Persona;
	}


	@Override
	public String toString() {
		return "Persona [id_Persona=" + id_Persona + ", nombre=" + nombre + ", apellido=" + apellido + ", email="
				+ email + ", telefono=" + telefono + ", getNombre()=" + getNombre() + ", getApellido()=" + getApellido()
				+ ", getEmail()=" + getEmail() + ", getTelefono()=" + getTelefono() + ", getId_Persona()="
				+ getId_Persona() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}
	
	
	
	

}
