package datos;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import static datos.Conexion.*;

import domain.Persona;


   public class PersonaDAO {
	
	private static final String SQL_SELECT ="SELECT id_persona,nombre,apellido,email,telefono FROM persona";
	private static final String SQL_INSERT="INSERT INTO persona(nombre, apellido, email, telefono) VALUES(?,?,?,?)";
	private static final String SQL_UPDATE="UPDATE persona SET nombre= ?, apellido= ?, email= ?, telefono= ? WHERE id_persona =? ";
	private static final String SQL_DELETE="DELETE FROM persona WHERE Id_persona=?";
	public List<Persona> seleccionar(){
		Connection conn=null;
		PreparedStatement stmt =null;
		ResultSet rs=null;
		Persona persona=null;
		List<Persona> personas =new ArrayList<>();
		
		try {
			conn=getConnection();
			stmt=conn.prepareStatement(SQL_SELECT);
			rs=stmt.executeQuery();
			while(rs.next()) {
				int idPersona=rs.getInt("id_persona");
				String nombre =rs.getString("nombre");
				String apellido=rs.getString("apellido");
				String email=rs.getString("email");
				int telefono=rs.getInt("telefono");
				
				persona =new Persona(idPersona,nombre,apellido,email,telefono);
				personas.add(persona);
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(System.out);
		}
		
		finally{
			try {
				close(conn);
				close(stmt);
				close(rs);
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.print(" error al cerrar la connection \n");
				e.printStackTrace(System.out);
			}
			
		}
		
		return personas;
		
	}
	public int insertar(Persona persona) {
		Connection conn=null;
		PreparedStatement stmt= null;
		int registros=0;
		try {
			conn=getConnection();
			stmt=conn.prepareStatement(SQL_INSERT);
			stmt.setString(1,persona.getNombre());
			stmt.setString(2,persona.getApellido());
			stmt.setString(3,persona.getEmail());
			stmt.setInt(4,persona.getTelefono());
			registros=stmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(System.out);
		}
		finally {
			try {
				close(stmt);
				close(conn);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		
		return registros;
				
		
	}
 
	public int actualizar(Persona persona) {
		Connection conn=null;
		PreparedStatement stmt = null;
		int registros=0;
		
		try {
			conn=getConnection();
			stmt=conn.prepareStatement(SQL_UPDATE);
			stmt.setString(1,persona.getNombre());
			stmt.setString(2,persona.getApellido());
			stmt.setString(3,persona.getEmail());
			stmt.setInt(4,persona.getTelefono());
			stmt.setInt(5,persona.getId_Persona());
			registros=stmt.executeUpdate();
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(System.out);
		}
		finally {
			try {
				close(stmt);
				close(conn);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		return 0;
		
		
	}
	
	public int eliminar(Persona persona) {
		Connection conn=null;
		PreparedStatement stmt = null;
		int registros=0;
		
		try {
			conn=getConnection();
			stmt=conn.prepareStatement(SQL_DELETE);
			stmt.setInt(1,persona.getId_Persona());
			registros=stmt.executeUpdate();
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(System.out);
		}
		finally {
			try {
				close(stmt);
				close(conn);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		return registros;
		
		
	}
	
}
