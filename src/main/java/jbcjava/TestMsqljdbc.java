package jbcjava;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TestMsqljdbc {

	public static void main(String[] args) {
		String url="jdbc:mysql://localhost:3306/tables?useSSL=false&useTiemezone=true&serverTimezone=UTC&allowPublicKeyRetrieval=true";
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection conexion =DriverManager.getConnection(url,"root","root");
			Statement instruccion=conexion.createStatement();
			var sql="SELECT Id_persona,nombre,apellido,email,telefono FROM persona";
			ResultSet resultado=instruccion.executeQuery(sql);
			while(resultado.next()){
				System.out.println("ID_persona : "+resultado.getInt("Id_persona")+" nombre : "+resultado.getString("nombre") + " apellido : "+resultado.getString("apellido"));
				
			}
			resultado.close();
			instruccion.close();
			conexion.close();
			
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(System.out);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
