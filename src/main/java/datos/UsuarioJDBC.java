package datos;

import static datos.Conexion.close;
import static datos.Conexion.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import domain.Usuario;

public class UsuarioJDBC {
	private static final String SQL_SELECT ="SELECT id_usuario,username, password FROM Usuario";
	private static final String SQL_INSERT="INSERT INTO Usuario(username, password) VALUES(?,?)";
	private static final String SQL_UPDATE="UPDATE Usuario SET username= ?, password= ? WHERE id_Usuario =? ";
	private static final String SQL_DELETE="DELETE FROM Usuario WHERE Id_Usuario=?";
	public List<Usuario> seleccionar(){
		Connection conn=null;
		PreparedStatement stmt =null;
		ResultSet rs=null;
		Usuario Usuario=null;
		List<Usuario> Usuarios =new ArrayList<>();		
		try {
			conn=getConnection();
			stmt=conn.prepareStatement(SQL_SELECT);
			rs=stmt.executeQuery();
			while(rs.next()) {
				int idUsuario=rs.getInt("id_Usuario");
				String username =rs.getString("username");
				String password=rs.getString("password");
				Usuario =new Usuario(idUsuario,username,password);
				Usuarios.add(Usuario);	
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(System.out);
		}
		
		finally{
			try {
				close(conn);
				close(stmt);
				close(rs);
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.print(" error al cerrar la connection \n");
				e.printStackTrace(System.out);
			}
			
		}
		
		return Usuarios;
		
	}
	public int insertar(Usuario Usuario) {
		Connection conn=null;
		PreparedStatement stmt= null;
		int registros=0;
		try {
			conn=getConnection();
			stmt=conn.prepareStatement(SQL_INSERT);
			stmt.setString(1,Usuario.getUsername());
			stmt.setString(2,Usuario.getPassword());
			registros=stmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(System.out);
		}
		finally {
			try {
				close(stmt);
				close(conn);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		
		return registros;
				
		
	}
 
	public int actualizar(Usuario Usuario) {
		Connection conn=null;
		PreparedStatement stmt = null;
		int registros=0;
		
		try {
			conn=getConnection();
			stmt=conn.prepareStatement(SQL_UPDATE);
			stmt.setString(1,Usuario.getUsername());
			stmt.setString(2,Usuario.getPassword());
			stmt.setInt(3,Usuario.getId_usuario());
			registros=stmt.executeUpdate();
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(System.out);
		}
		finally {
			try {
				close(stmt);
				close(conn);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		return 0;
		
		
	}
	
	public int eliminar(Usuario Usuario) {
		Connection conn=null;
		PreparedStatement stmt = null;
		int registros=0;
		
		try {
			conn=getConnection();
			stmt=conn.prepareStatement(SQL_DELETE);
			stmt.setInt(1,Usuario.getId_usuario());
			registros=stmt.executeUpdate();			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(System.out);
		}
		finally {
			try {
				close(stmt);
				close(conn);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		return registros;
		
		
	}
}
